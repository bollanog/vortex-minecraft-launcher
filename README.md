## Vortex Minecraft Launcher

Fast, lightweight and easy to use Minecraft launcher. Natively available for Windows and Linux.

---

## Features

* Lightweight and fast
* Open-Source
* Cross-Platform (available for Windows and Linux)
* Supports all Minecraft versions
* Supports Forge and other APIs
* Downloads all Minecraft versions
* Downloads missing libraries
* Doesn't require Minecraft account
* Doesn't require Java to work
* Can work fully offline

---

## Download

Check the [**Releases**](https://gitlab.com/Kron4ek/vortex-minecraft-launcher/-/releases) page to download the latest launcher version.

---

## Screenshots

![settings](https://i.imgur.com/dkiweug.png)
![main window](https://i.imgur.com/pd2tnnK.png)
![client downloader](https://i.imgur.com/1QTjiDw.png)

---

## License

[GPLv3](https://gitlab.com/Kron4ek/vortex-minecraft-launcher/blob/master/LICENSE)